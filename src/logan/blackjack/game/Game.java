package logan.blackjack.game;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import logan.blackjack.cards.Card;
import logan.blackjack.cards.CardDeck;
import logan.blackjack.cards.Hand;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ArrayList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.Toolkit;

@SuppressWarnings({"serial", "unused"})
public class Game extends JFrame {

	private double balance = 0.0;
	private ArrayList<JButton> dealerCardSlots = new ArrayList<JButton>();
	private ArrayList<JButton> playerCardSlots = new ArrayList<JButton>();
	private Hand playerHand;
	private Hand dealerHand;
	private String winner = "";
	private int betAmt = 1;
	private int betAmtFinal = 0;
	private int dealCount = 0;
	private CardDeck deck;
	private String gameLog;
	private Card[] deckArray;
	private int currentDeckPos = 0;
	private String cardTotal = "";
	private String dealerCardTotal = "";
	private JPanel contentPane;
	private JLabel lblDealer = new JLabel("DEALER");
	private JButton startButton = new JButton("START");
	private JTextField depositField;
	private JLabel lblDeposit = new JLabel("Deposit");
	private final JLabel lblBalance = new JLabel("Balance: " + balance);
	private final JLabel lblBetAmount = new JLabel("Bet Amount: " + betAmt);
	private final JLabel lblCardTotal = new JLabel("Card Total: " + cardTotal);
	private final JButton clearBet = new JButton("CLEAR BET");
	private final JButton add5 = new JButton("$5");
	private final JButton add50 = new JButton("$50");
	private final JButton add1 = new JButton("$1");
	private final JButton add10 = new JButton("$10");
	private final JButton add100 = new JButton("$100");
	private final JLabel lblDealerCardTotal = new JLabel("Card Total: " + dealerCardTotal);
	private final JButton dealerCard1 = new JButton("");
	private final JButton dealerCard2 = new JButton("");
	private final JButton dealerCard3 = new JButton("");
	private final JButton dealerCard4 = new JButton("");
	private final JButton dealerCard5 = new JButton("");
	private final JButton playerCard1 = new JButton("");
	private final JButton playerCard2 = new JButton("");
	private final JButton playerCard3 = new JButton("");
	private final JButton playerCard4 = new JButton("");
	private final JButton btnHit = new JButton("Hit");
	private final JButton btnDeal = new JButton("DEAL");
	private final JButton btnDouble = new JButton("Double");
	private final JButton btnStand = new JButton("STAND");
	private final JButton playerCard5 = new JButton("");
	private final JLabel lblOutput = new JLabel("Win");
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Game frame = new Game();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Game() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Game.class.getResource("/cards/Money Bags.png")));
		setVisible(true);
		setResizable(false);
		setTitle("Blackjack");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 720, 500);
		contentPane = new JPanel();
		contentPane.setForeground(Color.CYAN);
		contentPane.setBackground(new Color(204, 0, 51));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				startButton.setVisible(false);
				lblDeposit.setVisible(true);
				depositField.setVisible(true);
			}
		});
		lblOutput.setBackground(new Color(204, 0, 51));
		lblOutput.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				lblOutput.setVisible(false);
				if (lblOutput.getText().equals("GAME OVER!"))
				{
					System.exit(0);
				}
			}
		});
		
		lblOutput.setHorizontalAlignment(SwingConstants.CENTER);
		lblOutput.setFont(new Font("SWTOR Trajan", Font.PLAIN, 20));
		lblOutput.setBounds(200, 175, 300, 100);
		lblOutput.setVisible(false);
		contentPane.add(lblOutput);
		startButton.setBackground(new Color(204, 0, 51));
		startButton.setBorder(null);
		startButton.setFont(new Font("SWTOR Trajan", Font.BOLD, 64));
		startButton.setForeground(Color.GREEN);
		startButton.setBounds(200, 175, 300, 100);
		contentPane.add(startButton);
		
		
		lblDealer.setEnabled(true);
		lblDealer.setFont(new Font("SWTOR Trajan", Font.BOLD, 32));
		lblDealer.setHorizontalTextPosition(SwingConstants.CENTER);
		lblDealer.setHorizontalAlignment(SwingConstants.CENTER);
		lblDealer.setForeground(Color.DARK_GRAY);
		lblDealer.setBounds(230, 2, 250, 40);
		lblDealer.setVisible(false);
		contentPane.add(lblDealer);
		
		depositField = new JTextField();
		depositField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10)
				{
					setStartingBalance();
				}
			}
		});
		depositField.setFont(new Font("SWTOR Trajan", Font.PLAIN, 64));
		depositField.setHorizontalAlignment(SwingConstants.CENTER);
		depositField.setBounds(200, 175, 300, 100);
		depositField.setVisible(false);
		contentPane.add(depositField);
		depositField.setColumns(10);
		
		lblDeposit.setForeground(Color.ORANGE);
		lblDeposit.setHorizontalTextPosition(SwingConstants.CENTER);
		lblDeposit.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeposit.setFont(new Font("SWTOR Trajan", Font.BOLD, 30));
		lblDeposit.setBounds(200, 124, 300, 40);
		lblDeposit.setVisible(false);
		contentPane.add(lblDeposit);
		
		
		lblBalance.setForeground(Color.GREEN);
		lblBalance.setFont(new Font("SWTOR Trajan", Font.PLAIN, 20));
		lblBalance.setHorizontalAlignment(SwingConstants.LEFT);
		lblBalance.setBounds(10, 2, 210, 40);
		lblBalance.setVisible(false);
		contentPane.add(lblBalance);
		lblBetAmount.setFont(new Font("SWTOR Trajan", Font.PLAIN, 20));
		lblBetAmount.setForeground(Color.DARK_GRAY);
		lblBetAmount.setBounds(10, 421, 250, 50);
		lblBetAmount.setVisible(false);
		
		contentPane.add(lblBetAmount);
		lblCardTotal.setFont(new Font("SWTOR Trajan", Font.PLAIN, 20));
		lblCardTotal.setBounds(456, 385, 248, 40);
		lblCardTotal.setVisible(false);
		
		contentPane.add(lblCardTotal);
		clearBet.setBackground(new Color(204, 0, 51));
		clearBet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				betAmt = 1;
				lblBetAmount.setText("Bet Amount: " + betAmt);
			}
		});
		clearBet.setBorder(null);
		clearBet.setForeground(Color.RED);
		clearBet.setFont(new Font("SketchFlow Print", Font.PLAIN, 14));
		clearBet.setBounds(10, 324, 89, 23);
		clearBet.setVisible(false);
		
		contentPane.add(clearBet);
		add5.setBackground(new Color(204, 0, 51));
		add5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				betAmt = betAmt+5;
				lblBetAmount.setText("Bet Amount: " + betAmt);
			}
		});
		add5.setFont(new Font("SketchFlow Print", Font.PLAIN, 20));
		add5.setBorder(null);
		add5.setBounds(10, 359, 89, 23);
		add5.setVisible(false);
		
		contentPane.add(add5);
		add50.setBackground(new Color(204, 0, 51));
		add50.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				betAmt = betAmt+50;
				lblBetAmount.setText("Bet Amount: " + betAmt);
			}
		});
		add50.setFont(new Font("SketchFlow Print", Font.PLAIN, 20));
		add50.setBorder(null);
		add50.setBounds(10, 393, 89, 23);
		add50.setVisible(false);
		
		contentPane.add(add50);
		add1.setBackground(new Color(204, 0, 51));
		add1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				betAmt = betAmt+1;
				lblBetAmount.setText("Bet Amount: " + betAmt);
			}
		});
		add1.setFont(new Font("SketchFlow Print", Font.PLAIN, 20));
		add1.setBorder(null);
		add1.setBounds(109, 324, 89, 23);
		add1.setVisible(false);
		
		contentPane.add(add1);
		add10.setBackground(new Color(204, 0, 51));
		add10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				betAmt = betAmt+10;
				lblBetAmount.setText("Bet Amount: " + betAmt);
			}
		});
		add10.setFont(new Font("SketchFlow Print", Font.PLAIN, 20));
		add10.setBorder(null);
		add10.setBounds(109, 359, 89, 23);
		add10.setVisible(false);
		
		contentPane.add(add10);
		add100.setBackground(new Color(204, 0, 51));
		add100.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				betAmt = betAmt+100;
				lblBetAmount.setText("Bet Amount: " + betAmt);
			}
		});
		add100.setFont(new Font("SketchFlow Print", Font.PLAIN, 20));
		add100.setBorder(null);
		add100.setBounds(109, 393, 89, 23);
		add100.setVisible(false);
		
		contentPane.add(add100);
		lblDealerCardTotal.setFont(new Font("SWTOR Trajan", Font.PLAIN, 20));
		lblDealerCardTotal.setBounds(456, 5, 248, 40);
		lblDealerCardTotal.setVisible(false);
		
		contentPane.add(lblDealerCardTotal);
		
		JPanel cardDisplay = new JPanel();
		cardDisplay.setBackground(new Color(204, 0, 51));
		cardDisplay.setBorder(null);
		cardDisplay.setBounds(343, 53, 361, 300);
		contentPane.add(cardDisplay);
		cardDisplay.setLayout(null);
		dealerCard1.setBackground(new Color(204, 0, 51));
		
		dealerCard1.setBounds(0, 0, 73, 97);
		dealerCard1.setVisible(false);
		cardDisplay.add(dealerCard1);
		dealerCard3.setBackground(new Color(204, 0, 51));
		
		dealerCard3.setBounds(146, 0, 73, 97);
		dealerCard3.setVisible(false);
		dealerCard2.setBackground(new Color(204, 0, 51));
		
		dealerCard2.setBounds(73, 0, 73, 97);
		dealerCard2.setVisible(false);
		cardDisplay.add(dealerCard2);
		cardDisplay.add(dealerCard3);
		dealerCard4.setBackground(new Color(204, 0, 51));
		
		dealerCard4.setBounds(219, 0, 73, 97);
		dealerCard4.setVisible(false);
		cardDisplay.add(dealerCard4);
		dealerCard5.setBackground(new Color(204, 0, 51));
		
		dealerCard5.setBounds(292, 0, 73, 97);
		dealerCard5.setVisible(false);
		cardDisplay.add(dealerCard5);
		playerCard1.setBackground(new Color(204, 0, 51));
		
		playerCard1.setBounds(0, 203, 73, 97);
		playerCard1.setVisible(false);
		cardDisplay.add(playerCard1);
		playerCard2.setBackground(new Color(204, 0, 51));
		
		playerCard2.setBounds(73, 203, 73, 97);
		playerCard2.setVisible(false);
		cardDisplay.add(playerCard2);
		playerCard3.setBackground(new Color(204, 0, 51));
		
		playerCard3.setBounds(146, 203, 73, 97);		
		playerCard3.setVisible(false);
		cardDisplay.add(playerCard3);
		playerCard4.setBackground(new Color(204, 0, 51));
		
		playerCard4.setBounds(219, 203, 73, 97);	
		playerCard4.setVisible(false);
		cardDisplay.add(playerCard4);
		playerCard5.setBackground(new Color(204, 0, 51));
		
		playerCard5.setBounds(292, 203, 73, 97);
		playerCard5.setVisible(false);
		cardDisplay.add(playerCard5);
		btnHit.setBackground(new Color(204, 0, 51));
		btnHit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hit();
			}
		});
		
		btnHit.setForeground(Color.YELLOW);
		btnHit.setFont(new Font("SWTOR Trajan", Font.BOLD, 24));
		btnHit.setBounds(30, 90, 120, 40);
		btnHit.setVisible(false);
		contentPane.add(btnHit);
		btnDeal.setBackground(new Color(204, 0, 51));
		btnDeal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dealHand();
			}
		});
		btnDeal.setForeground(Color.ORANGE);
		btnDeal.setFont(new Font("SWTOR Trajan", Font.BOLD, 20));
		btnDeal.setBounds(30, 250, 120, 40);
		btnDeal.setVisible(false);
		
		contentPane.add(btnDeal);
		btnStand.setBackground(new Color(204, 0, 51));
		btnStand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStand.setVisible(false);
				btnHit.setVisible(false);
				stand();
			}
		});
		btnStand.setForeground(Color.DARK_GRAY);
		btnStand.setFont(new Font("SWTOR Trajan", Font.BOLD, 20));
		btnStand.setBounds(30, 137, 120, 40);
		btnStand.setVisible(false);
		contentPane.add(btnStand);
		
		
		btnDouble.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Double();
			}
		});
		
		btnDouble.setBackground(new Color(204, 0 , 51));
		btnDouble.setForeground(Color.CYAN);
		btnDouble.setFont(new Font("SWTOR Trajan", Font.BOLD, 18));
		btnDouble.setBounds(30, 183, 120, 40);
		btnDouble.setVisible(false);
		contentPane.add(btnDouble);
	}
	
	
	public void setStartingBalance()
	{
		double depositAmt = Double.parseDouble(depositField.getText());
		balance = depositAmt;
		System.out.println("Balance set to: " + balance);
		depositField.setVisible(false);
		lblDeposit.setVisible(false);
		startGame();
	}
	
	public void startGame()
	{
		lblDealer.setVisible(true);
		lblBalance.setVisible(true);
		lblBalance.setText("Balance: " + balance);
		lblBetAmount.setVisible(true);
		lblCardTotal.setVisible(true);
		add1.setVisible(true);
		add5.setVisible(true);
		add10.setVisible(true);
		add50.setVisible(true);
		add100.setVisible(true);
		clearBet.setVisible(true);
		lblDealerCardTotal.setVisible(true);
		dealerCard1.setVisible(true);
		dealerCard2.setVisible(true);
		dealerCard3.setVisible(true);
		dealerCard4.setVisible(true);
		dealerCard5.setVisible(true);
		playerCard1.setVisible(true);
		playerCard2.setVisible(true);
		playerCard3.setVisible(true);
		playerCard4.setVisible(true);
		playerCard5.setVisible(true);
		fillCardSlots();
		getNewDeck();
		btnDeal.setVisible(true);
		
		
	}
	
	private void fillCardSlots()
	{
		dealerCardSlots.add(dealerCard1);
		dealerCardSlots.add(dealerCard2);
		dealerCardSlots.add(dealerCard3);
		dealerCardSlots.add(dealerCard4);
		dealerCardSlots.add(dealerCard5);
		playerCardSlots.add(playerCard1);
		playerCardSlots.add(playerCard2);
		playerCardSlots.add(playerCard3);
		playerCardSlots.add(playerCard4);
		playerCardSlots.add(playerCard5);
	}
	
	public void setDeck(CardDeck deckIn)
	{
		deck = deckIn;
		deckArray = deck.getCards();
	}
	
	private String imageFileName(Card c) {
		String str = "/cards/";
		if (c == null) {
			return "/cards/back1.GIF";
		}
		str += c.getNum() + c.getType().toLowerCase();
		str += ".GIF";
		return str;
	}
	
	private void logHand()
	{
		gameLog += "Hand " + dealCount + ": ";
		for (Card c : dealerHand.getCards())
		{
			gameLog += c.getNum() + " of " + c.getType() + " ";
		}
		gameLog += dealerHand.getCardTotalOutput() + " | ";
		for (Card c : playerHand.getCards())
		{
			gameLog += c.getNum() + " of " + c.getType() + " ";
		}
		gameLog += playerHand.getCardTotalOutput() + "\n";
	}
	
	private void saveGameLog()
	{
		String logOutput = gameLog;
		
		try
		{
			FileOutputStream fos = new FileOutputStream("GameLog.dat");
			DataOutputStream dos = new DataOutputStream(fos);
			
			dos.writeBytes(logOutput);
			dos.flush();
			dos.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void clearCardSlots()
	{
		for (int i = 0; i < 5; i++)
		{
			String cardImageFileName = imageFileName(null);
			URL imageURL = getClass().getResource(cardImageFileName);
			if (imageURL != null)
			{
				ImageIcon icon = new ImageIcon(imageURL);
				dealerCardSlots.get(i).setIcon(icon);
				playerCardSlots.get(i).setIcon(icon);
			}
			else
			{
				throw new RuntimeException("Card image not found: \"" + cardImageFileName + "\"");
			}
		}
	}
	
	private void setDealerCards(ArrayList<Card> dealerHand2)
	{
		for (int i = 0; i < dealerHand2.size(); i++)
		{
			String cardImageFileName = imageFileName(dealerHand2.get(i));
			URL imageURL = getClass().getResource(cardImageFileName);
			if (imageURL != null)
			{
				ImageIcon icon = new ImageIcon(imageURL);
				dealerCardSlots.get(i).setIcon(icon);
			}
			else
			{
				throw new RuntimeException("Card image not found: \"" + cardImageFileName + "\"");
			}
		}
	}
	
	private void setPlayerCards(ArrayList<Card> playerHand2)
	{
		for (int i = 0; i < playerHand2.size(); i++)
		{
			String cardImageFileName = imageFileName(playerHand2.get(i));
			URL imageURL = getClass().getResource(cardImageFileName);
			if (imageURL != null)
			{
				ImageIcon icon = new ImageIcon(imageURL);
				playerCardSlots.get(i).setIcon(icon);
			}
			else
			{
				throw new RuntimeException("Card image not found: \"" + cardImageFileName + "\"");
			}
		}
	}
	
	private void getNewDeck()
	{
		CardDeck newDeck = new CardDeck();
		newDeck.shuffleDeck();
		setDeck(newDeck);
		printDeck();
	}
	
	private void checkDeck()
	{
		if (currentDeckPos == 51)
		{
			getNewDeck();
			currentDeckPos = 0;
		}
	}
	
	private void printDeck()
	{
		int x = 0;
		for (Card i : deck.getCards())
		{
			if (i.getNum() == 1)
			{
				System.out.print("Ace Of " + i.getType() + " | ");
			}
			else if (i.getNum() == 11)
			{
				System.out.print("Jack Of " + i.getType() + " | ");
			}
			else if (i.getNum() == 12)
			{
				System.out.print("Queen Of " + i.getType() + " | ");
			}
			else if (i.getNum() == 13)
			{
				System.out.print("King Of " + i.getType() + " | ");
			}
			else
			{
				System.out.print(i.getNum()+" Of "+i.getType() + " | ");
			}
			if (x == 8)
			{
				System.out.println();
				x=0;
			}
			x++;
		}
		System.out.println("\n");
	}
	
	
	private void bet()
	{
		betAmtFinal = betAmt;
		if (balance >= betAmtFinal)
		{
			balance -= betAmtFinal;
			lblBalance.setText("Balance: " + balance);
		}
		else if (balance < betAmtFinal)
		{
			lblOutput.setText("Can't Bet That Much");
			lblOutput.setVisible(true);
			betAmt = 1;
		}
	}
	
	public void refreshGameScreen()
	{
		lblCardTotal.setText("Card Total: " + playerHand.getCardTotalOutput());
		lblDealerCardTotal.setText("Card Total: " + dealerHand.getCardTotalOutput());
		setDealerCards(dealerHand.getCards());
		setPlayerCards(playerHand.getCards());
	}
	
	private void clearScreen()
	{
		lblDealer.setVisible(false);
		lblBalance.setVisible(false);
		lblBetAmount.setVisible(false);
		lblCardTotal.setVisible(false);
		clearBet.setVisible(false);
		add5.setVisible(false);
		add50.setVisible(false);
		add1.setVisible(false);
		add10.setVisible(false);
		add100.setVisible(false);
		lblDealerCardTotal.setVisible(false);
		dealerCard1.setVisible(false);
		dealerCard2.setVisible(false);
		dealerCard3.setVisible(false);
		dealerCard4.setVisible(false);
		dealerCard5.setVisible(false);
		playerCard1.setVisible(false);
		playerCard2.setVisible(false);
		playerCard3.setVisible(false);
		playerCard4.setVisible(false);
		playerCard5.setVisible(false);
		btnDeal.setVisible(false);
		btnHit.setVisible(false);
		btnStand.setVisible(false);
	}
	
	private void dealHand()
	{
		lblOutput.setVisible(false);
		if (balance == 0)
		{
			lblOutput.setText("GAME OVER!");
			lblOutput.setVisible(true);
			saveGameLog();
			clearScreen();
		}
		else
		{
			if (validateBet() == true)
			{
				clearCardSlots();
				dealCount++;
				lblCardTotal.setText("Card Total: ");
				lblDealerCardTotal.setText("Card Total: ");
				bet();
				checkDeck();
				playerHand = new Hand(deckArray[currentDeckPos]);
				currentDeckPos++;
				checkDeck();
				dealerHand = new Hand(deckArray[currentDeckPos]);
				currentDeckPos++;
				checkDeck();
				playerHand.addCard(deckArray[currentDeckPos]);
				currentDeckPos++;
				checkDeck();
				refreshGameScreen();
				btnDeal.setVisible(false);
				btnHit.setVisible(true);
				btnStand.setVisible(true);
				if (balance >= betAmtFinal*2)
				{
					btnDouble.setVisible(true);
				}
			}
		}
	}
	
	private boolean validateBet()
	{
		betAmtFinal = betAmt;
		if (balance < betAmtFinal)
		{
			lblOutput.setText("Can't Bet That Much");
			lblOutput.setVisible(true);
			betAmt = 1;
			lblBetAmount.setText("Bet Amount: " + betAmt);
			return false;
		}
		return true;
	}
	
	private void hit()
	{
		btnDouble.setVisible(false);
		checkDeck();
		playerHand.addCard(deckArray[currentDeckPos]);
		currentDeckPos++;
		checkDeck();
		if (playerHand.getFinalScore() > 21)
		{
			btnHit.setVisible(false);
			getWinner();
			resetGame();
		}
		refreshGameScreen();	
		
	}
	
	private void stand()
	{
		btnDouble.setVisible(false);
		checkDeck();
		dealerHand.addCard(deckArray[currentDeckPos]);
		currentDeckPos++;
		checkDeck();
		refreshGameScreen();
		dealerPlay();
		getWinner();
		resetGame();
	}
	
	private void Double()
	{
		balance -= betAmtFinal;
		betAmtFinal*= 2;
		checkDeck();
		playerHand.addCard(deckArray[currentDeckPos]);
		currentDeckPos++;
		checkDeck();
		if (playerHand.getFinalScore() > 21)
		{
			btnHit.setVisible(false);
			getWinner();
			resetGame();
		}
		else 
		{
			checkDeck();
			dealerHand.addCard(deckArray[currentDeckPos]);
			currentDeckPos++;
			checkDeck();
			refreshGameScreen();
			dealerPlay();
			getWinner();
			resetGame();
		}
	}
	
	private void dealerPlay()
	{
		while(dealerHand.getFinalScore() < 17)
		{
			checkDeck();
			dealerHand.addCard(deckArray[currentDeckPos]);
			currentDeckPos++;
			checkDeck();
			refreshGameScreen();
		}
	}
	
	private void getWinner()
	{
		if (dealerHand.getFinalScore() > 21)
		{
			winner = "player";
			balance += betAmtFinal*2;
			lblBalance.setText("Balance: " + balance);
			lblOutput.setText("Player Wins");
			lblOutput.setVisible(true);
			logHand();
		}
		else if (playerHand.getFinalScore() > 21)
		{
			winner = "dealer";
			lblOutput.setText("Dealer Wins");
			lblOutput.setVisible(true);
			logHand();
		}
		else if (playerHand.getFinalScore() > dealerHand.getFinalScore() && playerHand.getFinalScore() <= 21)
		{
			winner = "player";
			balance += betAmtFinal*2;
			lblBalance.setText("Balance: " + balance);
			lblOutput.setText("Player Wins");
			lblOutput.setVisible(true);
			logHand();
		}
		else if (dealerHand.getFinalScore() > playerHand.getFinalScore() && dealerHand.getFinalScore() <= 21)
		{
			winner = "dealer";
			lblOutput.setText("Dealer Wins");
			lblOutput.setVisible(true);
			logHand();
		}
		else
		{
			winner = "push";
			balance += betAmtFinal;
			lblBalance.setText("Balance: " + balance);
			lblOutput.setText("Push");
			lblOutput.setVisible(true);
			logHand();
		}
	}
	
	private void resetGame()
	{
		btnHit.setVisible(false);
		btnStand.setVisible(false);
		btnDouble.setVisible(false);
		btnDeal.setVisible(true);
	}
}
