package logan.blackjack.cards;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class CardDeck {

	static Card[] cards = new Card[52];
	
	public CardDeck()
	{
		generateDeck();
	}
	
	public void generateDeck()
	{
		String[] type = {"Spades", "Hearts", "Diamonds", "Clubs"};

		int i = 0;

		for (int x = 1; x <=13; x++)
		{
			for (int y = 0; y<=3; y++)
			{
				//System.out.println(i+"."+x+"."+y);
				cards[i] = new Card(x, type[y]);
				i++;
			}
		}

	}
	
	public void shuffleDeck()
	{
		Random rnd = ThreadLocalRandom.current();
	    for (int i = cards.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);

	      Card a = cards[index];
	      cards[index] = cards[i];
	      cards[i] = a;
	    }
	}
	
	
	public Card[] getCards()
	{
		return cards;
	}
	
}
