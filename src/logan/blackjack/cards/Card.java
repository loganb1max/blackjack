package logan.blackjack.cards;

public class Card {

	private int num;
	private String type;
	private int weight;
	
	
	public Card(int numI, String typeI)
	{
		num = numI;
		type = typeI;
		setWeight();
	}
	
	
	public int getNum()
	{
		return num;
	}
	
	public String getType()
	{
		return type;
	}

	private void setWeight()
	{
		if (num > 10)
		{
			weight = 10;
		}
		else
		{
			weight = num;
		}
	}

	public int getWeight() {
		return weight;
	}

	
}
