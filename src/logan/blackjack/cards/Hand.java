package logan.blackjack.cards;

import java.util.ArrayList;

public class Hand {

	private ArrayList<Card> cards = new ArrayList<Card>();
	private int handTotalLow = 0;
	private int handTotalHigh = 0;
	private boolean containsAce = false;
	private boolean playHigh = false;
	
	public Hand(Card startingCard)
	{
		cards.add(startingCard);
		totalHand();
	}
	
	private void totalHand()
	{
		checkForAce();
		if (containsAce == true)
		{
			totalHandLow();
			totalHandHigh();
		}
		else
		{
			totalHandLow();
		}
	}
	
	private void checkForAce()
	{
		for (Card c : cards)
		{
			if (c.getWeight() == 1)
			{
				containsAce = true;
				playHigh = true;
			}
		}
	}
	
	private void totalHandLow()
	{
		handTotalLow = 0;
		for (Card c : cards)
		{
			handTotalLow += c.getWeight();
		}
	}
	
	private void totalHandHigh()
	{
		handTotalHigh = 0;
		for (Card c : cards)
		{
			if (c.getWeight() == 1)
			{
				handTotalHigh += 11;
			}
			else
			{
				handTotalHigh += c.getWeight();
			}
		}
	}
	
	public void addCard(Card c)
	{
		cards.add(c);;
		totalHand();
		if (handTotalHigh > 21)
		{
			playHigh = false;
		}
	}
	
	public String getCardTotalOutput()
	{
		if (playHigh ==  true)
		{
			return handTotalLow + " / " + handTotalHigh;
		}
		else
		{
			return handTotalLow + "";
		}
	}
	
	public int getFinalScore()
	{
		if (playHigh == true)
		{
			return handTotalHigh;
		}
		else
		{
			return handTotalLow;
		}
	}
	
	public ArrayList<Card> getCards()
	{
		return cards;
	}
	
}
